var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { gotoPage } from "../../router.js";
import { RegisterModel, RegisterController, RegisterView, } from "../register/index.js";
import { ProjectsController, ProjectsModel, ProjectsView, } from "../../todos/projects/index.js";
import { TasksController, TasksModel, TasksView, } from "../../todos/tasks/index.js";
import { UserModel } from "../userModel.js";
export class LoginController {
    constructor(model, view, id) {
        this.model = model;
        this.view = view;
        this.id = id;
        this.view.render();
        this.goToRegisterPage();
        this.bindEventLogin();
    }
    bindEventLogin() {
        const form = this.view.getForm();
        console.log(form);
        form.onsubmit = (e) => {
            e.preventDefault();
            this.view.validateInputs();
            if (!form.classList.contains("invalid")) {
                const login = new UserModel(this.view.getUsernameValue(), this.view.getPasswordValue());
                const check = login.loginUser();
                check.then((data) => __awaiter(this, void 0, void 0, function* () {
                    console.log(data);
                    if (data) {
                        alert("successful");
                        const data = yield new ProjectsModel().projectByUserLogin();
                        if (data.length !== 0) {
                            const id = data[data.length - 1].id;
                            gotoPage(".todos", `${id}`, () => __awaiter(this, void 0, void 0, function* () {
                                new ProjectsController(new ProjectsModel(), new ProjectsView(id), id);
                                new TasksController(new TasksModel(), new TasksView(id), id);
                            }));
                        }
                        else {
                            gotoPage(".todos", "todos", () => {
                                new ProjectsController(new ProjectsModel(), new ProjectsView(this.id), this.id);
                                new TasksController(new TasksModel(), new TasksView(this.id), this.id);
                            });
                        }
                        const remove = document.querySelector(".login");
                        remove === null || remove === void 0 ? void 0 : remove.remove();
                    }
                    else {
                        alert("Username or password incorrect");
                    }
                }));
            }
        };
    }
    goToRegisterPage() {
        const btn = document.getElementById("goToRegister");
        btn.addEventListener("click", (e) => {
            e.preventDefault();
            gotoPage(".login", "register", () => {
                new RegisterController(new RegisterModel(), new RegisterView(this.id), this.id);
            });
            const remove = document.querySelector(".login");
            remove === null || remove === void 0 ? void 0 : remove.remove();
        });
    }
}
