var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { DBService } from "../dbService/db.js";
export class UserModel {
    constructor(username, password) {
        this.username = username;
        this.password = password;
        this.newDb = new DBService("development");
    }
    get() {
        const user = this.newDb.findOne("USERS", { username: this.username });
        return user
            .then((data) => {
            if (data) {
                return data;
            }
        })
            .catch((e) => {
            console.log(e);
        });
    }
    checkPassword(password, passwordDb) {
        if (passwordDb === password) {
            return true;
        }
        return false;
    }
    getUserLogin() {
        const user = localStorage.getItem("USERLOGIN")
            ? JSON.parse(localStorage.getItem("USERLOGIN"))
            : [];
        if (user) {
            return user;
        }
        else
            return false;
    }
    registerUser() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.get();
            console.log(data);
            if (data === undefined) {
                this.newDb.saveOne("USERS", this);
                return true;
            }
            return false;
        });
    }
    loginUser() {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.get();
            if (user) {
                if (this.checkPassword(this.password, user.password) == true) {
                    localStorage.setItem("USERLOGIN", JSON.stringify(user));
                    return true;
                }
                else
                    return false;
            }
            return false;
        });
    }
    logoutUser() {
        localStorage.setItem("USERLOGIN", JSON.stringify(""));
    }
}
