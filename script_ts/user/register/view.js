import { gotoPage } from "../../router.js";
export class RegisterView {
    constructor(id) {
        this.id = id;
    }
    template() {
        return `
        <form class="form form-register" name="form" id="form-${this.id}" novalidate>
			<legend class="title-form-login">Register</legend>
			<div class="form-input">
				<div class="form-group">
					<label class="form-label">Username:</label>
					<input
						name="username"
						id="username"
						type="text"
						class="username form-control"
						required=true
						minlength = "5"
						maxlength = "10"
					/>
					<div class="error-messages single-error">
						<span class="error-message required-error"> Please enter this field !</span>
						<span class="error-message minlength-error">Length must be more than 5 characters<span>
                        <span class="error-message maxlength-error">Length must be more than 10 characters<span>
					</div>
				</div>

				<div class="form-group">
					<label class="form-label">Password:</label>
					<input
						type="password"
						name="password"
						class="password form-control"
						id="password"
						required=true
						minlength = "5"
						maxlength = "9"
					/>
					<div class="error-messages single-error">
						<span class="error-message required-error"> Please enter this field !</span>
						<span class="error-message minlength-error">Length must be more than 5 characters<span>
                        <span class="error-message maxlength-error">Length must be more than 9 characters<span>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label" >Confirm password:</label>
					<input
						type="password"
						name="re-password"
						class="re-password form-control"
						id="re-password"
						required=true
						minlength = "5"
						maxlength = "9"
					/>
					<div class="error-messages single-error">
						<span class="error-message required-error"> Please enter this field !</span>
						<span class="error-message minlength-error">Length must be more than 5 characters<span>
                        <span class="error-message maxlength-error">Length must be more than 9 characters<span>
					</div>
				</div>
			</div>
			<button class="submit-form" type="submit">Submit</button>
			<a class="link_login" id="goToLogin">Login<a/>
		</form>
        `;
    }
    render() {
        let app = document.querySelector("main");
        let formRegister = document.createElement("section");
        formRegister.classList.add("register", `register-form-${this.id}`, "page");
        app = app.appendChild(formRegister);
        app.innerHTML = this.template();
        gotoPage(".register");
    }
    validateInputs() {
        const form = document.querySelector(`#form-${this.id}`);
        const inputElements = form.querySelectorAll('input');
        inputElements.forEach(inputElement => {
            return this.validate(inputElement);
        });
    }
    getUsernameValue() {
        const username = document.querySelector("#username").value;
        return username;
    }
    getPasswordValue() {
        const password = document.querySelector("#password").value;
        return password;
    }
    getRePasswordValue() {
        const rePassword = document.querySelector("#re-password").value;
        return rePassword;
    }
    getForm() {
        const form = document.querySelector(`#form-${this.id}`);
        return form;
    }
    validate(inputElement) {
        let hassError = false;
        const errorElement = inputElement.nextElementSibling;
        if (inputElement.getAttribute('required')) {
            if (!inputElement.value) {
                hassError = true;
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.add('required-invalid');
            }
            else {
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.remove('required-invalid');
            }
        }
        if (inputElement.getAttribute('minlength')) {
            const minlength = parseInt(inputElement.getAttribute('minlength'));
            if (inputElement.value.length < minlength) {
                hassError = true;
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.add('minlength-invalid');
            }
            else {
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.remove('minlength-invalid');
            }
        }
        if (inputElement.getAttribute('maxlength')) {
            const maxlength = parseInt(inputElement.getAttribute('maxlength'));
            if (inputElement.value.length > maxlength) {
                hassError = true;
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.add('maxlength-invalid');
            }
            else {
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.remove('maxlength-invalid');
            }
        }
        if (hassError) {
            errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.add('has-error');
        }
        else {
            errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.remove('has-error');
        }
        this.checkForm(this.getForm());
    }
    checkForm(form) {
        let valid = true;
        const errorElements = form.querySelectorAll('.error-messages');
        errorElements.forEach(errorElement => {
            valid = valid && !errorElement.classList.contains('has-error');
        });
        if (!valid) {
            form.classList.add('invalid');
        }
        else {
            form.classList.remove('invalid');
        }
        return valid;
    }
}
