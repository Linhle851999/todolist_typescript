import { RegisterModel } from "./model.js";
import { RegisterView } from "./view.js";
import { RegisterController } from "./controller.js";
export { RegisterModel, RegisterView, RegisterController };
