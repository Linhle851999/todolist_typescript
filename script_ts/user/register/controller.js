import { gotoPage } from "../../router.js";
import { LoginView, LoginController, LoginModel } from "../login/index.js";
import { UserModel } from "../userModel.js";
export class RegisterController {
    constructor(model, view, id) {
        this.bindEventRegister = () => {
            const form = this.view.getForm();
            form.onsubmit = (e) => {
                e.preventDefault();
                this.view.validateInputs();
                if (!form.classList.contains('invalid')) {
                    if (this.view.getPasswordValue() === this.view.getRePasswordValue()) {
                        const register = new UserModel(this.view.getUsernameValue(), this.view.getPasswordValue());
                        register.registerUser().then((data) => {
                            if (data) {
                                alert("successful");
                                gotoPage(".register", "login", () => {
                                    new LoginController(new LoginModel(), new LoginView(this.id), this.id);
                                });
                                const remove = document.querySelector(".register");
                                remove === null || remove === void 0 ? void 0 : remove.remove();
                            }
                            else {
                                alert("username already exists");
                            }
                        });
                    }
                    else {
                        alert("Comfirm passwords did not match");
                    }
                }
            };
        };
        this.goToLoginPage = () => {
            const btn = document.getElementById("goToLogin");
            btn.addEventListener("click", (e) => {
                e.preventDefault();
                gotoPage(".register", "login", () => {
                    const login = new LoginController(new LoginModel(), new LoginView(this.id), this.id);
                });
                const remove = document.querySelector(".register");
                remove === null || remove === void 0 ? void 0 : remove.remove();
            });
        };
        this.model = model;
        this.view = view;
        this.id = id;
        this.view.render();
        this.bindEventRegister();
        this.goToLoginPage();
    }
}
