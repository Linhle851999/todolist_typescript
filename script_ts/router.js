const gotoPage = (pageSelector, id, callback) => {
    const pages = document.querySelectorAll('.page');
    pages.forEach(element => {
        element.classList.remove('active');
    });
    const page = document.querySelector(pageSelector);
    page === null || page === void 0 ? void 0 : page.classList.add('active');
    if (id) {
        window.history.replaceState({}, document.title, `?id=${id}`);
    }
    callback && callback();
};
export { gotoPage };
