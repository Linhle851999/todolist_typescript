export var EPriority;
(function (EPriority) {
    EPriority["High"] = "High Priority";
    EPriority["Medium"] = "Medium Priority";
    EPriority["Low"] = "Low Priority";
})(EPriority || (EPriority = {}));
