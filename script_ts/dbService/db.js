const localStorageService = {
    save: (resource, data) => {
        return new Promise((rs) => {
            localStorage.setItem(resource, JSON.stringify(data));
            rs();
        });
    },
    get: (resource) => {
        return new Promise((rs) => {
            const localStorageData = localStorage.getItem(resource);
            const data = localStorageData ? JSON.parse(localStorageData) : [];
            rs(data);
        });
    },
};
export class DBService {
    constructor(env) {
        this.getlist = (resource) => {
            const db = this.env == "development" ? localStorageService : {};
            const data = db.get(resource);
            return data;
        };
        this.findMany = (resource, item) => {
            return new Promise((rs) => {
                const db = this.env == "development" ? localStorageService : {};
                db.get(resource).then((data) => {
                    const dataFilter = [];
                    data.filter((a) => {
                        const keys = Object.keys(a);
                        keys.map((key) => {
                            if (a[key] === item[key]) {
                                dataFilter.push(a);
                            }
                        });
                    });
                    rs(dataFilter);
                });
            });
        };
        this.findOne = (resource, item) => {
            return new Promise((rs, rj) => {
                const db = this.env == "development" ? localStorageService : {};
                db.get(resource).then((data) => {
                    data.find((a) => {
                        const keys = Object.keys(a);
                        keys.map((key) => {
                            if (a[key] === item[key]) {
                                rs(a);
                            }
                        });
                    });
                    rj();
                });
            });
        };
        this.saveOne = (resource, item) => {
            return new Promise((rs) => {
                const db = this.env == "development" ? localStorageService : {};
                db.get(resource).then((data) => {
                    if (data) {
                        data.push(item);
                    }
                    db.save(resource, data);
                    rs();
                });
            });
        };
        this.deleteOneById = (resource, id) => {
            return new Promise((rs, rj) => {
                const db = this.env == "development" ? localStorageService : {};
                const data = db.get(resource);
                const items = data.filter((item) => item.id != id);
                if (items) {
                    db.save(resource, items);
                    rs(true);
                }
                else {
                    rj(false);
                }
            });
        };
        this.updateOne = (resource, item) => {
            return new Promise((rs, rj) => {
                if (item) {
                    this.saveOne(resource, item);
                    rs(true);
                }
                else {
                    rj(false);
                }
            });
        };
        this.env = env;
    }
}
