var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { UserModel } from "../user/userModel.js";
import { DBService } from "../dbService/db.js";
export class TodosModel {
    constructor() {
        this.userLogin = new UserModel().getUserLogin().username;
        this.newDb = new DBService("development");
        this.listProject = this.newDb.getlist("PROJECTS") || [];
    }
    save(data) {
        localStorage.setItem("PROJECTS", JSON.stringify(data));
        return true;
    }
    addProject(projectName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (projectName) {
                    const data = yield this.listProject;
                    data.push({
                        id: "_" + Math.random().toString(36),
                        projectName: projectName,
                        userLogin: this.userLogin,
                    });
                    this.save(data);
                }
            }
            catch (err) {
                console.log(err);
            }
        });
    }
    addTask(idProject, taksName, time, priority, description) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const tasks = (yield this.newDb.getlist("TASKS")) || [];
                if (taksName && time && priority && description) {
                    tasks.push({
                        idProject: idProject,
                        id: "_" + Math.random().toString(36),
                        taksName: taksName,
                        time: time,
                        priority: priority,
                        description: description,
                    });
                    localStorage.setItem("TASKS", JSON.stringify(tasks));
                }
            }
            catch (err) {
                console.log(err);
            }
        });
    }
    findTasksByIdProject(idProject) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const rs = yield ((_a = this.newDb) === null || _a === void 0 ? void 0 : _a.findMany("TASKS", {
                    id: idProject,
                }));
                if (rs) {
                    return rs;
                }
                else {
                    throw new Error("error");
                }
            }
            catch (err) {
                throw err;
            }
        });
    }
    projectByUserLogin() {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const rs = yield ((_a = this.newDb) === null || _a === void 0 ? void 0 : _a.findMany("PROJECTS", {
                    userLogin: this.userLogin,
                }));
                if (rs) {
                    return rs;
                }
                else {
                    throw new Error("error");
                }
            }
            catch (err) {
                throw err;
            }
        });
    }
    findProject(nameProject) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const rs = yield ((_a = this.newDb) === null || _a === void 0 ? void 0 : _a.findOne("PROJECTS", {
                    projectName: nameProject,
                }));
                if (rs) {
                    return rs;
                }
                else
                    throw new Error("error");
            }
            catch (err) {
                throw err;
            }
        });
    }
    findProjectById(id) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const rs = yield ((_a = this.newDb) === null || _a === void 0 ? void 0 : _a.findOne("PROJECTS", {
                    id: id,
                }));
                if (rs) {
                    return rs;
                }
                else
                    throw new Error("error");
            }
            catch (err) {
                throw err;
            }
        });
    }
}
