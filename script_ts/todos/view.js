var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { gotoPage } from "../router.js";
import { projectTemplate, tasksTemplate } from "./template.js";
export class TodosView {
    constructor(id) {
        this.id = id;
    }
    template() {
        return `
        <div class="sidebar-projects">
					<h2 class="title-projects">Projects</h2>
					<button class="btn btn-newProject">New Project</button>
					<div class="modal"></div>
					<div class="item-projects"></div>
				</div>
				<div class="tasks"></div>
        `;
    }
    render() {
        let app = document.querySelector("main");
        let totos = document.createElement("section");
        totos.classList.add("todos", "page");
        app = app.appendChild(totos);
        app.innerHTML = this.template();
        gotoPage(".todos");
    }
    addbtnLogout() {
        let app = document.querySelector("header");
        app.innerHTML += `
		<i class="fa fa-sign-out" id="btn-logout"></i>
		`;
    }
    renderList(e) {
        const element = document.querySelector(".item-projects");
        element.innerHTML = e
            .slice()
            .reverse()
            .map((project) => projectTemplate(project, this.id))
            .join("");
    }
    renderListTask(e) {
        const element = document.querySelector(".group-tasks");
        console.log(element);
        element.innerHTML = e.map((task) => tasksTemplate(task)).join("");
    }
    addNewproject(e) {
        return __awaiter(this, void 0, void 0, function* () {
            const element = document.querySelector(".item-projects");
            const project = yield e;
            element.innerHTML += projectTemplate(project, this.id);
        });
    }
    tasksTemplate(projectName) {
        const element = document.querySelector(".tasks");
        element.innerHTML = tasksTemplate(projectName);
    }
    getValueProjectName() {
        const projectName = document.querySelector("#input-nameProject").value;
        return projectName;
    }
    validate(inputElement) {
        let hassError = false;
        const errorElement = inputElement.nextElementSibling;
        if (inputElement.getAttribute("required")) {
            if (!inputElement.value) {
                hassError = true;
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.add("required-invalid");
            }
            else {
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.remove("required-invalid");
            }
        }
        if (inputElement.getAttribute("minlength")) {
            const minlength = parseInt(inputElement.getAttribute("minlength"));
            if (inputElement.value.length < minlength) {
                hassError = true;
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.add("minlength-invalid");
            }
            else {
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.remove("minlength-invalid");
            }
        }
        if (hassError) {
            errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.add("has-error");
        }
        else {
            errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.remove("has-error");
        }
        this.checkForm(this.getForm());
    }
    getForm() {
        const form = document.querySelector(`#form`);
        return form;
    }
    checkForm(form) {
        let valid = true;
        const errorElements = form.querySelectorAll(".error-messages");
        errorElements.forEach((errorElement) => {
            valid = valid && !errorElement.classList.contains("has-error");
        });
        if (!valid) {
            form.classList.add("invalid");
        }
        else {
            form.classList.remove("invalid");
        }
        return valid;
    }
    validateInputs() {
        const form = document.querySelector(`#form`);
        const inputElements = form.querySelectorAll("input");
        inputElements.forEach((inputElement) => {
            return this.validate(inputElement);
        });
        const inputElement = form.querySelectorAll("textarea");
        inputElement.forEach((inputElement) => {
            return this.validate(inputElement);
        });
    }
}
