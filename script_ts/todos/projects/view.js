var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { gotoPage } from "../../router.js";
import { projectTemplate } from "../template.js";
export class ProjectsView {
    constructor(id) {
        this.id = id;
    }
    template() {
        return `
		<div class="sidebar-projects">
			<h2 class="title-projects">Projects</h2>
			<button class="btn btn-newProject">New Project</button>
			<div class="modal"></div>
			<div class="item-projects"></div>
		</div>
		<div class="tasks"></div>
		`;
    }
    render() {
        let app = document.querySelector("main");
        let totos = document.createElement("section");
        totos.classList.add("todos", "page");
        app = app.appendChild(totos);
        app.innerHTML = this.template();
        gotoPage(".todos");
    }
    getValueProjectName() {
        const projectName = document.querySelector("#input-nameProject").value;
        return projectName;
    }
    renderList(e) {
        const element = document.querySelector(".item-projects");
        console.log(element);
        element.innerHTML = e
            .slice()
            .reverse()
            .map((project) => projectTemplate(project, this.id))
            .join("");
    }
    addNewproject(e) {
        return __awaiter(this, void 0, void 0, function* () {
            const element = document.querySelector(".item-projects");
            const project = yield e;
            element.innerHTML += projectTemplate(project, this.id);
        });
    }
    addbtnLogout() {
        let app = document.querySelector("header");
        app.innerHTML += `
		<i class="fa fa-sign-out" id="btn-logout"></i>
		`;
    }
}
