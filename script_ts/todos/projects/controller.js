var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { DBService } from "../../dbService/db.js";
import { modalAddProject } from "../template.js";
import { gotoPage } from "../../router.js";
import { UserModel } from "../../user/userModel.js";
import { LoginModel, LoginView, LoginController, } from "../../user/login/index.js";
export class ProjectsController {
    constructor(model, view, id) {
        this.model = model;
        this.view = view;
        this.id = id;
        this.newDb = new DBService("development");
        this.view.render();
        this.view.addbtnLogout();
        this.renderListProject();
        this.openModelAddProject();
        this.bindEventLogout();
    }
    renderListProject() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = (yield this.model.projectByUserLogin());
            this.view.renderList(data);
        });
    }
    openModelAddProject() {
        const modal = document.querySelector(".modal");
        const btn = document.querySelector(".btn-newProject");
        btn === null || btn === void 0 ? void 0 : btn.addEventListener("click", () => {
            modal.innerHTML = modalAddProject();
            modal === null || modal === void 0 ? void 0 : modal.classList.toggle("show-modal");
            this.addProject();
        });
        window.addEventListener("click", (event) => {
            if (event.target === modal) {
                modal === null || modal === void 0 ? void 0 : modal.classList.toggle("show-modal");
            }
        });
    }
    bindEventLogout() {
        const btn = document.getElementById("btn-logout");
        btn.addEventListener("click", (e) => {
            e.preventDefault();
            const remove = document.querySelector(".todos");
            console.log(remove);
            remove === null || remove === void 0 ? void 0 : remove.remove();
            new UserModel().logoutUser();
            const btnLogout = document.querySelector("#btn-logout");
            btnLogout === null || btnLogout === void 0 ? void 0 : btnLogout.remove();
            gotoPage(".register", "login", () => {
                new LoginController(new LoginModel(), new LoginView(this.id), this.id);
            });
        });
    }
    addProject() {
        const form = document.querySelector(".form-addproject");
        const modal = document.querySelector(".modal");
        form.onsubmit = (e) => __awaiter(this, void 0, void 0, function* () {
            e.preventDefault();
            yield this.model.addProject(this.view.getValueProjectName());
            const newP = this.model.findProject(this.view.getValueProjectName());
            this.view.addNewproject(newP);
            modal === null || modal === void 0 ? void 0 : modal.classList.toggle("show-modal");
            alert("successful");
        });
    }
}
