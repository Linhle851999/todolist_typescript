import { listTasks, tasksTemplate } from "../template.js";
export class TasksView {
    constructor(id) {
        this.id = id;
    }
    tasksTemplate(projectName) {
        const element = document.querySelector(".tasks");
        element.innerHTML = tasksTemplate(projectName);
    }
    renderListTask(e) {
        const element = document.querySelector(".group-tasks");
        console.log(e);
        element.innerHTML = e.map((task) => listTasks(task)).join("");
    }
    validate(inputElement) {
        let hassError = false;
        const errorElement = inputElement.nextElementSibling;
        if (inputElement.getAttribute("required")) {
            if (!inputElement.value) {
                hassError = true;
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.add("required-invalid");
            }
            else {
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.remove("required-invalid");
            }
        }
        if (inputElement.getAttribute("minlength")) {
            const minlength = parseInt(inputElement.getAttribute("minlength"));
            if (inputElement.value.length < minlength) {
                hassError = true;
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.add("minlength-invalid");
            }
            else {
                errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.remove("minlength-invalid");
            }
        }
        if (hassError) {
            errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.add("has-error");
        }
        else {
            errorElement === null || errorElement === void 0 ? void 0 : errorElement.classList.remove("has-error");
        }
        this.checkForm(this.getForm());
    }
    getForm() {
        const form = document.querySelector(`#form`);
        return form;
    }
    checkForm(form) {
        let valid = true;
        const errorElements = form.querySelectorAll(".error-messages");
        errorElements.forEach((errorElement) => {
            valid = valid && !errorElement.classList.contains("has-error");
        });
        if (!valid) {
            form.classList.add("invalid");
        }
        else {
            form.classList.remove("invalid");
        }
        return valid;
    }
    validateInputs() {
        const form = document.querySelector(`#form`);
        const inputElements = form.querySelectorAll("input");
        inputElements.forEach((inputElement) => {
            return this.validate(inputElement);
        });
        const inputElement = form.querySelectorAll("textarea");
        inputElement.forEach((inputElement) => {
            return this.validate(inputElement);
        });
    }
}
