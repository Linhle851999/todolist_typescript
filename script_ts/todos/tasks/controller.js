var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { DBService } from "../../dbService/db.js";
import { modalAddTask, modalDetailTask } from "../template.js";
import { ProjectsModel } from "../projects/model.js";
import { UserModel } from "../../user/userModel.js";
import { gotoPage } from "../../router.js";
import { LoginModel, LoginController, LoginView, } from "../../user/login/index.js";
export class TasksController {
    constructor(model, view, id) {
        this.bindEventAddTask = () => {
            const form = this.view.getForm();
            console.log(form);
            form.onsubmit = (e) => {
                const taskname = document.querySelector(".input-nameaddtask").value;
                const taskdate = document.querySelector(".input-dateaddtask").value;
                console.log(typeof taskdate);
                const taskpriority = document.querySelector(".select-priority").value;
                const taskdesciption = document.querySelector(".textarea-description").value;
                e.preventDefault();
                this.view.validateInputs();
                if (!form.classList.contains("invalid")) {
                    this.model.addTask(this.id, taskname, taskdate, taskpriority, taskdesciption);
                    alert("oke rồi nhé");
                }
            };
        };
        this.handleControlButton = () => {
            const btn = document.querySelector(".item-projects");
            btn.addEventListener("click", this.handleTaskButtonClick);
        };
        this.handleControlButtonTask = () => {
            const btnTask = document.querySelector(".group-tasks");
            console.log(btnTask);
            btnTask.addEventListener("click", this.handleTaskButtonClick);
        };
        this.handleTaskButtonClick = (event) => {
            const target = event.target;
            const id = target.dataset.id;
            console.log(id);
            switch (true) {
                case target.matches(".name-project-task"):
                    const value = target.innerHTML;
                    gotoPage(".todos", `${id}`, () => {
                        this.view.tasksTemplate(value);
                        this.listTasks(id);
                    });
                    break;
                case target.matches(".btn-detail"):
                    this.openModalDetailTask(id);
                    break;
            }
        };
        this.model = model;
        this.view = view;
        this.id = id;
        this.newDb = new DBService("development");
        this.listTasks(this.id);
        this.handleControlButton();
    }
    listTasks(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const value = yield new ProjectsModel().findProjectById(id);
            this.view.tasksTemplate(value.projectName);
            this.view.renderListTask(yield this.model.findTasksByIdProject(id));
            this.openModelAddTask();
            this.handleControlButtonTask();
        });
    }
    openModelAddTask() {
        const modal = document.querySelector(".modal-addTask");
        const btn = document.querySelector(".btn-newTask");
        console.log(btn);
        btn.addEventListener("click", () => {
            modal.innerHTML = modalAddTask();
            modal.classList.toggle("show-modal");
            this.bindEventAddTask();
        });
        window.addEventListener("click", (event) => {
            if (event.target === modal) {
                modal === null || modal === void 0 ? void 0 : modal.classList.toggle("show-modal");
            }
        });
    }
    openModalDetailTask(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const modal = document.querySelector(".modal-detailtask");
            const task = yield this.model.findTaskByid(id);
            modal.innerHTML = modalDetailTask(task);
            modal.classList.toggle("show-modal");
            window.addEventListener("click", (event) => {
                if (event.target === modal) {
                    modal === null || modal === void 0 ? void 0 : modal.classList.toggle("show-modal");
                }
            });
        });
    }
    bindEventLogout() {
        const btn = document.getElementById("btn-logout");
        btn.addEventListener("click", (e) => {
            e.preventDefault();
            const remove = document.querySelector(".todos");
            remove === null || remove === void 0 ? void 0 : remove.remove();
            new UserModel().logoutUser();
            const btnLogout = document.querySelector("#btn-logout");
            btnLogout === null || btnLogout === void 0 ? void 0 : btnLogout.remove();
            gotoPage(".register", "login", () => {
                new LoginController(new LoginModel(), new LoginView(this.id), this.id);
            });
        });
    }
}
