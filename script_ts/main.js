var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { LoginController, LoginView, LoginModel } from "./user/login/index.js";
import { RegisterController, RegisterView, RegisterModel, } from "./user/register/index.js";
import { ProjectsController, ProjectsModel, ProjectsView, } from "./todos/projects/index.js";
import { TasksController, TasksModel, TasksView } from "./todos/tasks/index.js";
const headerViewTemplate = () => {
    return `
        <div class="yourtodo-header">
            <img src="./image/icon-header.png">
            <div class="yourtodo">TodoList</div>
        </div>
    `;
};
class Todo {
    constructor(id) {
        this.id = id;
        let app = document.querySelector("main");
        let element = document.createElement("header");
        element.className = "header-todolist";
        app = app.appendChild(element);
        app.innerHTML = headerViewTemplate();
        this.run();
    }
    run() {
        let location = window.location.href.split("/index.html").pop();
        let locationId = location.split("").slice(4).join("");
        if (!location) {
            this.login(this.id);
        }
        else if (location === `?id=${locationId}`) {
            if (locationId == "login") {
                return this.login(locationId);
            }
            else if (locationId == "register") {
                return this.register(locationId);
            }
            else
                return this.todos(locationId);
        }
    }
    login(id) {
        new LoginController(new LoginModel(), new LoginView(id), id);
    }
    register(id) {
        new RegisterController(new RegisterModel(), new RegisterView(id), id);
    }
    todos(id) {
        return __awaiter(this, void 0, void 0, function* () {
            new ProjectsController(new ProjectsModel(), new ProjectsView(id), id);
            new TasksController(new TasksModel(), new TasksView(id), id);
        });
    }
}
const TodoAppp = new Todo("root");
