import { LoginController, LoginView, LoginModel } from "./user/login/index.js";
import {
	RegisterController,
	RegisterView,
	RegisterModel,
} from "./user/register/index.js";
import {
	ProjectsController,
	ProjectsModel,
	ProjectsView,
} from "./todos/projects/index.js";
import { TasksController, TasksModel, TasksView } from "./todos/tasks/index.js";
const headerViewTemplate = () => {
	return `
        <div class="yourtodo-header">
            <img src="./image/icon-header.png">
            <div class="yourtodo">TodoList</div>
        </div>
    `;
};

interface ITodo {
	id: string;
	run: () => void;
}

class Todo implements ITodo {
	id: string;
	constructor(id: string) {
		this.id = id;
		let app = document.querySelector("main") as HTMLDivElement;
		let element = document.createElement("header") as HTMLDivElement;
		element.className = "header-todolist";
		app = app.appendChild(element);
		app.innerHTML = headerViewTemplate();
		this.run();
	}
	run() {
		let location = window.location.href.split("/index.html").pop() as string;
		let locationId = location.split("").slice(4).join("");
		if (!location) {
			this.login(this.id);
		} else if (location === `?id=${locationId}`) {
			if (locationId == "login") {
				return this.login(locationId);
			} else if (locationId == "register") {
				return this.register(locationId);
			} else return this.todos(locationId);
		}
	}

	login(id: string) {
		new LoginController(new LoginModel(), new LoginView(id), id);
	}
	register(id: string) {
		new RegisterController(new RegisterModel(), new RegisterView(id), id);
	}
	async todos(id: string) {
		new ProjectsController(new ProjectsModel(), new ProjectsView(id), id);
		new TasksController(new TasksModel(), new TasksView(id), id);
	}
}

const TodoAppp = new Todo("root");
