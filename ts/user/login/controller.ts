import { LoginModel } from "./model.js";
import { LoginView } from "./view.js";
import { gotoPage } from "../../router.js";
import {
	RegisterModel,
	RegisterController,
	RegisterView,
} from "../register/index.js";
import {
	ProjectsController,
	ProjectsModel,
	ProjectsView,
} from "../../todos/projects/index.js";
import {
	TasksController,
	TasksModel,
	TasksView,
} from "../../todos/tasks/index.js";
import { UserModel } from "../userModel.js";
interface ILoginController {
	id: string;
	model: LoginModel;
	view: LoginView;

	bindEventLogin: () => void;
	goToRegisterPage: () => void;
}
export class LoginController implements ILoginController {
	id: string;
	model: LoginModel;
	view: LoginView;

	constructor(model: LoginModel, view: LoginView, id: string) {
		this.model = model;
		this.view = view;
		this.id = id;

		this.view.render();
		this.goToRegisterPage();
		this.bindEventLogin();
	}
	bindEventLogin() {
		const form = this.view.getForm();
		console.log(form);
		form.onsubmit = (e: Event) => {
			e.preventDefault();
			this.view.validateInputs();
			if (!form.classList.contains("invalid")) {
				const login = new UserModel(
					this.view.getUsernameValue(),
					this.view.getPasswordValue()
				);
				const check = login.loginUser();
				check.then(async (data) => {
					console.log(data);
					if (data) {
						alert("successful");
						const data = await new ProjectsModel().projectByUserLogin();
						if (data.length !== 0) {
							const id = data[data.length - 1].id;
							gotoPage(".todos", `${id}`, async () => {
								new ProjectsController(
									new ProjectsModel(),
									new ProjectsView(id!),
									id!
								);
								new TasksController(new TasksModel(), new TasksView(id!), id!);
							});
						} else {
							gotoPage(".todos", "todos", () => {
								new ProjectsController(
									new ProjectsModel(),
									new ProjectsView(this.id),
									this.id
								);
								new TasksController(
									new TasksModel(),
									new TasksView(this.id),
									this.id
								);
							});
						}
						const remove = document.querySelector(".login");
						remove?.remove();
					} else {
						alert("Username or password incorrect");
					}
				});
			}
		};
	}
	goToRegisterPage() {
		const btn = document.getElementById("goToRegister") as HTMLElement;
		btn.addEventListener("click", (e) => {
			e.preventDefault();
			gotoPage(".login", "register", () => {
				new RegisterController(
					new RegisterModel(),
					new RegisterView(this.id),
					this.id
				);
			});
			const remove = document.querySelector(".login");
			remove?.remove();
		});
	}
}
