import { IView } from "../../base/index.js";
import { gotoPage } from "../../router.js";
interface ILoginView {
	id: string;
}
export class LoginView implements ILoginView, IView {
	id: string;
	constructor(id: string) {
		this.id = id;
	}
	template() {
		return `
      <form class="form form-login" name="form" id="form-${this.id}" novalidate>
				<legend class="title-form-login">Login</legend>
				<div class="form-input">
					<div class="form-group">
						<label class="form-label">Username:</label>
						<input
							name="username"
							id="username"
							type="text"
							class="username form-control"
							required=true
						/>
            <div class="error-messages single-error">
              <span class="error-message required-error"> Please enter this field !</span>
            </div>
					</div>

					<div class="form-group">
						<label class="form-label">Password:</label>
						<input
							type="password"
							name="password"
							class="password form-control"
							id="password"
							required=true
						/>
            <div class="error-messages single-error">
              <span class="error-message required-error"> Please enter this field !</span>
            </div>
					</div>
				</div>
				<button class="submit-form" type="submit">Submit</button>
          <a class="link_register" id="goToRegister">Register<a/>
		    </form>
        `;
	}
	render() {
		let app = document.querySelector("main") as HTMLElement;
		let formLogin = document.createElement("section")! as HTMLElement;
		formLogin.classList.add("login", `login-form-${this.id}`, "page");
		app = app.appendChild(formLogin);
		app.innerHTML = this.template();

		gotoPage(".login");
	}

	validateInputs() {
		const form = document.querySelector(`#form-${this.id}`) as HTMLFormElement;
		const inputElements = form.querySelectorAll("input");
		inputElements.forEach((inputElement) => {
			return this.validate(inputElement);
		});
	}
	getUsernameValue() {
		const username = (document.querySelector("#username") as HTMLInputElement)
			.value;
		return username;
	}
	getPasswordValue() {
		const password = (document.querySelector("#password") as HTMLInputElement)
			.value;
		return password;
	}
	getForm() {
		const form = document.querySelector(`#form-${this.id}`) as HTMLFormElement;
		return form;
	}
	validate(inputElement: HTMLInputElement) {
		let hassError = false;
		const errorElement = inputElement.nextElementSibling;

		if (inputElement.getAttribute("required")) {
			if (!inputElement.value) {
				hassError = true;
				errorElement?.classList.add("required-invalid");
			} else {
				errorElement?.classList.remove("required-invalid");
			}
		}

		if (hassError) {
			errorElement?.classList.add("has-error");
		} else {
			errorElement?.classList.remove("has-error");
		}
		this.checkForm(this.getForm());
	}
	checkForm(form: HTMLFormElement) {
		let valid = true;
		const errorElements = form.querySelectorAll(".error-messages");

		errorElements.forEach((errorElement) => {
			valid = valid && !errorElement.classList.contains("has-error");
		});

		if (!valid) {
			form.classList.add("invalid");
		} else {
			form.classList.remove("invalid");
		}
		return valid;
	}
}
