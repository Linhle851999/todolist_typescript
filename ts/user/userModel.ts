import { IListModel, IModel, IUser } from "../base/index.js";
import { DBService } from "../dbService/db.js";
interface IUserModel {
	checkPassword: (password: string, passwordDb: string) => boolean;
	getUserLogin: () => Promise<void>;
	loginUser: () => Promise<boolean>;
	logoutUser: () => void;
	registerUser: () => Promise<boolean>;
}
export class UserModel implements IUserModel, IModel<IUser> {
	username?: string;
	password?: string;
	newDb?: DBService<IUser>;
	constructor(username?: string, password?: string) {
		this.username = username;
		this.password = password;
		this.newDb = new DBService<IUser>("development");
	}
	get() {
		const user = this.newDb!.findOne("USERS", { username: this.username });
		return user
			.then((data: Partial<IUser>) => {
				if (data) {
					return data;
				}
			})
			.catch((e) => {
				console.log(e);
			});
	}
	checkPassword(password: string, passwordDb: string) {
		if (passwordDb === password) {
			return true;
		}
		return false;
	}
	getUserLogin() {
		const user = localStorage.getItem("USERLOGIN")
			? JSON.parse(localStorage.getItem("USERLOGIN")!)
			: [];
		if (user) {
			return user;
		} else return false;
	}
	async registerUser() {
		const data = await this.get();
		console.log(data)
		if (data === undefined) {
			this.newDb!.saveOne("USERS", this);
			return true;
		}
		return false;
	}
	async loginUser() {
		const user = await this.get();
		if (user) {
			if (this.checkPassword(this.password!, user.password!) == true) {
				localStorage.setItem("USERLOGIN", JSON.stringify(user));	
				return true;
			} else return false;
		}
		return false;
	}
	logoutUser() {
		localStorage.setItem("USERLOGIN", JSON.stringify(""));
	}
}
