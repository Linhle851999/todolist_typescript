import { RegisterModel , RegisterView} from "./index.js";
import {gotoPage} from "../../router.js"
import {LoginView, LoginController, LoginModel} from "../login/index.js"
import { UserModel } from "../userModel.js";
interface IRegisterController {
	id: string;
	model: RegisterModel;
	view: RegisterView;
}
export class RegisterController implements IRegisterController {
	id: string;
	model: RegisterModel;
	view: RegisterView;

	constructor(model: RegisterModel, view: RegisterView, id: string) {
		this.model = model;
		this.view = view;
		this.id = id;

		this.view.render();
		this.bindEventRegister()
		this.goToLoginPage()
	}
	bindEventRegister = ()=>{
		const form = this.view.getForm()
		form.onsubmit = (e: Event) =>{
			e.preventDefault()
			this.view.validateInputs()
			if (!form.classList.contains('invalid')) {
				if(this.view.getPasswordValue() === this.view.getRePasswordValue()){
					const register = new UserModel(this.view.getUsernameValue(), this.view.getPasswordValue())
					register.registerUser().then((data)=>{
						if(data){
							alert("successful")
							gotoPage(".register", "login", () => {
								new LoginController(new LoginModel(), new LoginView(this.id),this.id)
							})
							const remove = document.querySelector(".register")
							remove?.remove()
						}else{
							alert("username already exists")
						}
					})
				} else{
					alert("Comfirm passwords did not match")
				}
            }
		}	
	}
	goToLoginPage = ()=>{
		const btn = document.getElementById("goToLogin") as HTMLElement
		btn.addEventListener("click",  (e) =>{
			e.preventDefault()
			gotoPage(".register", "login", () => {
				const login = new LoginController(new LoginModel(), new LoginView(this.id),this.id)
			})
			const remove = document.querySelector(".register")
			remove?.remove()
		})
	}
}
