import { TasksModel } from "./model.js";
import { TasksView } from "./view.js";
import { TasksController } from "./controller.js";

export { TasksController, TasksModel, TasksView };
