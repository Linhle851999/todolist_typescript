import { DBService } from "../../dbService/db.js";
import { TasksModel } from "./model.js";
import { modalAddTask, modalDetailTask } from "../template.js";
import { TasksView } from "./view.js";
import { ProjectsModel } from "../projects/model.js";
import { ITask } from "../../base/task.js";
import { UserModel } from "../../user/userModel.js";
import { gotoPage } from "../../router.js";
import {
	LoginModel,
	LoginController,
	LoginView,
} from "../../user/login/index.js";
interface ITasks {
	id: string;
	model: TasksModel;
	view: TasksView;
}
export class TasksController implements ITasks {
	model: TasksModel;
	view: TasksView;
	id: string;
	newDb: DBService<ITask>;
	constructor(model: TasksModel, view: TasksView, id: string) {
		this.model = model;
		this.view = view;
		this.id = id;
		this.newDb = new DBService<ITask>("development");

		this.listTasks(this.id);
		this.handleControlButton();
	}
	async listTasks(id: string) {
		const value = await new ProjectsModel().findProjectById(id!);
		this.view.tasksTemplate(value.projectName!);
		this.view.renderListTask(await this.model.findTasksByIdProject(id));
		this.openModelAddTask();
		this.handleControlButtonTask();
	}
	openModelAddTask() {
		const modal = document.querySelector(".modal-addTask") as HTMLElement;
		const btn = document.querySelector(".btn-newTask") as HTMLElement;
		console.log(btn);
		btn.addEventListener("click", () => {
			modal.innerHTML = modalAddTask();
			modal.classList.toggle("show-modal");
			this.bindEventAddTask();
		});
		window.addEventListener("click", (event) => {
			if (event.target === modal) {
				modal?.classList.toggle("show-modal");
			}
		});
	}
	async openModalDetailTask(id: string) {
		const modal = document.querySelector(".modal-detailtask") as HTMLElement;
		const task = await this.model.findTaskByid(id);
		modal.innerHTML = modalDetailTask(task);
		modal.classList.toggle("show-modal");

		window.addEventListener("click", (event) => {
			if (event.target === modal) {
				modal?.classList.toggle("show-modal");
			}
		});
	}
	bindEventLogout() {
		const btn = document.getElementById("btn-logout") as HTMLElement;
		btn.addEventListener("click", (e) => {
			e.preventDefault();
			const remove = document.querySelector(".todos");
			remove?.remove();
			new UserModel().logoutUser();
			const btnLogout = document.querySelector("#btn-logout");
			btnLogout?.remove();
			gotoPage(".register", "login", () => {
				new LoginController(new LoginModel(), new LoginView(this.id), this.id);
			});
		});
	}
	bindEventAddTask = () => {
		const form = this.view.getForm();
		console.log(form);
		form.onsubmit = (e: Event) => {
			const taskname = (
				document.querySelector(".input-nameaddtask") as HTMLInputElement
			).value;
			const taskdate = (
				document.querySelector(".input-dateaddtask") as HTMLInputElement
			).value;
			console.log(typeof taskdate);
			const taskpriority = (
				document.querySelector(".select-priority") as HTMLSelectElement
			).value;
			const taskdesciption = (
				document.querySelector(".textarea-description") as HTMLTextAreaElement
			).value;
			e.preventDefault();
			this.view.validateInputs();
			if (!form.classList.contains("invalid")) {
				this.model.addTask(
					this.id,
					taskname,
					taskdate,
					taskpriority,
					taskdesciption
				);
				alert("oke rồi nhé");
			}
		};
	};
	handleControlButton = () => {
		const btn = document.querySelector(".item-projects")! as HTMLElement;
		btn.addEventListener("click", this.handleTaskButtonClick);
	};
	handleControlButtonTask = () => {
		const btnTask = document.querySelector(".group-tasks")! as HTMLElement;
		console.log(btnTask);
		btnTask.addEventListener("click", this.handleTaskButtonClick);
	};
	handleTaskButtonClick = (event: Event) => {
		const target = event.target! as HTMLElement;
		const id = target.dataset.id!;
		console.log(id);
		switch (true) {
			case target.matches(".name-project-task"):
				const value = target.innerHTML;
				gotoPage(".todos", `${id}`, () => {
					this.view.tasksTemplate(value);
					this.listTasks(id);
				});
				break;
			case target.matches(".btn-detail"):
				this.openModalDetailTask(id);
				break;
		}
	};
}
