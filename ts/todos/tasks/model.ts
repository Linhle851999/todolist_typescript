import { ITask } from "../../base/index.js";
import { IProject } from "../../base/task.js";
import { DBService } from "../../dbService/db.js";
interface ITasksModel {
	addTask: (
		id: string,
		taksName: string,
		time: string,
		priority: string,
		description: string
	) => void;
	findTasksByIdProject: (idProject: string) => Promise<Partial<ITask>[]>;
	findTaskByid: (idProject: string) => Promise<Partial<ITask>>;
}
export class TasksModel implements ITasksModel {
	newDb?: DBService<ITask>;
	constructor() {
		this.newDb = new DBService<IProject>("development");
	}
	async addTask(
		idProject: string,
		taksName: string,
		time: string,
		priority: string,
		description: string
	) {
		try {
			const tasks = (await this.newDb!.getlist("TASKS")) || [];
			if (taksName && time && priority && description) {
				tasks.push({
					idProject: idProject,
					id: "_" + Math.random().toString(36),
					taksName: taksName,
					time: time,
					priority: priority,
					description: description,
				});
				localStorage.setItem("TASKS", JSON.stringify(tasks));
			}
		} catch (err) {
			console.log(err);
		}
	}
	async findTasksByIdProject(idProject: string) {
		try {
			const rs = await this.newDb?.findMany("TASKS", {
				idProject: idProject,
			});
			if (rs) {
				return rs;
			} else {
				throw new Error("error");
			}
		} catch (err) {
			throw err;
		}
	}
	async findTaskByid(id: string) {
		try {
			const rs = await this.newDb?.findOne("TASKS", {
				id: id,
			});
			if (rs) {
				return rs;
			} else {
				throw new Error("error");
			}
		} catch (err) {
			throw err;
		}
	}
}
