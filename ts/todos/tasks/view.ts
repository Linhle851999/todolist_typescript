import { ITask } from "../../base/task.js";
import { IListView } from "../../base/view.js";
import { listTasks, tasksTemplate } from "../template.js";
interface ITasksView {}
export class TasksView implements ITasksView, IListView<ITask> {
	id: string;
	constructor(id: string) {
		this.id = id;
	}
	tasksTemplate(projectName: string) {
		const element = document.querySelector(".tasks")! as HTMLElement;
		element.innerHTML = tasksTemplate(projectName);
	}
	renderListTask(e: Partial<ITask>[]) {
		const element = document.querySelector(".group-tasks")! as HTMLElement;
		console.log(e);
		element.innerHTML = e.map((task) => listTasks(task)).join("");
	}
	validate(inputElement: HTMLInputElement | HTMLTextAreaElement) {
		let hassError = false;
		const errorElement = inputElement.nextElementSibling;
		if (inputElement.getAttribute("required")) {
			if (!inputElement.value) {
				hassError = true;
				errorElement?.classList.add("required-invalid");
			} else {
				errorElement?.classList.remove("required-invalid");
			}
		}
		if (inputElement.getAttribute("minlength")) {
			const minlength = parseInt(inputElement.getAttribute("minlength")!);
			if (inputElement.value.length < minlength) {
				hassError = true;
				errorElement?.classList.add("minlength-invalid");
			} else {
				errorElement?.classList.remove("minlength-invalid");
			}
		}
		if (hassError) {
			errorElement?.classList.add("has-error");
		} else {
			errorElement?.classList.remove("has-error");
		}
		this.checkForm(this.getForm());
	}
	getForm() {
		const form = document.querySelector(`#form`) as HTMLFormElement;
		return form;
	}
	checkForm(form: HTMLFormElement) {
		let valid = true;
		const errorElements = form.querySelectorAll(".error-messages");

		errorElements.forEach((errorElement) => {
			valid = valid && !errorElement.classList.contains("has-error");
		});

		if (!valid) {
			form.classList.add("invalid");
		} else {
			form.classList.remove("invalid");
		}
		return valid;
	}
	validateInputs() {
		const form = document.querySelector(`#form`) as HTMLFormElement;
		const inputElements = form.querySelectorAll("input");
		inputElements.forEach((inputElement) => {
			return this.validate(inputElement);
		});
		const inputElement = form.querySelectorAll("textarea");
		inputElement.forEach((inputElement: HTMLTextAreaElement) => {
			return this.validate(inputElement);
		});
	}
}
