import { ProjectsModel } from "./model.js";
import { ProjectsView } from "./view.js";
import { ProjectsController } from "./controller.js";

export { ProjectsController, ProjectsModel, ProjectsView };
