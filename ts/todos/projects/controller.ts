import { DBService } from "../../dbService/db.js";
import { ProjectsModel } from "./model.js";
import { ProjectsView } from "./view.js";
import { IProject } from "../../base/task.js";
import { modalAddProject } from "../template.js";
import { gotoPage } from "../../router.js";
import { UserModel } from "../../user/userModel.js";
import {
	LoginModel,
	LoginView,
	LoginController,
} from "../../user/login/index.js";
interface IProjects {
	id: string;
	model: ProjectsModel;
	view: ProjectsView;
}
export class ProjectsController implements IProjects {
	model: ProjectsModel;
	view: ProjectsView;
	id: string;
	newDb: DBService<IProject>;
	constructor(model: ProjectsModel, view: ProjectsView, id: string) {
		this.model = model;
		this.view = view;
		this.id = id;
		this.newDb = new DBService<IProject>("development");

		this.view.render();
		this.view.addbtnLogout();
		this.renderListProject();
		this.openModelAddProject();
		this.bindEventLogout();
	}
	async renderListProject() {
		const data = (await this.model.projectByUserLogin()) as [IProject];
		this.view.renderList(data);
	}
	openModelAddProject() {
		const modal = document.querySelector(".modal") as HTMLElement;
		const btn = document.querySelector(".btn-newProject") as HTMLElement;
		btn?.addEventListener("click", () => {
			modal.innerHTML = modalAddProject();
			modal?.classList.toggle("show-modal");
			this.addProject();
		});
		window.addEventListener("click", (event) => {
			if (event.target === modal) {
				modal?.classList.toggle("show-modal");
			}
		});
	}
	bindEventLogout() {
		const btn = document.getElementById("btn-logout") as HTMLElement;
		btn.addEventListener("click", (e) => {
			e.preventDefault();
			const remove = document.querySelector(".todos");
			console.log(remove);
			remove?.remove();
			new UserModel().logoutUser();
			const btnLogout = document.querySelector("#btn-logout");
			btnLogout?.remove();
			gotoPage(".register", "login", () => {
				new LoginController(new LoginModel(), new LoginView(this.id), this.id);
			});
		});
	}
	addProject() {
		const form = document.querySelector(".form-addproject")! as HTMLElement;
		const modal = document.querySelector(".modal") as HTMLElement;
		form.onsubmit = async (e: Event) => {
			e.preventDefault();
			await this.model.addProject(this.view.getValueProjectName());
			const newP = this.model.findProject(this.view.getValueProjectName());
			this.view.addNewproject(newP);
			modal?.classList.toggle("show-modal");
			alert("successful");
		};
	}
}
