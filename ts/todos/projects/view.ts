import { IProject } from "../../base/task";
import { gotoPage } from "../../router.js";
import { IListView } from "../../base/view";
import { projectTemplate } from "../template.js";

interface IProjectsView {
	id: string;
}
export class ProjectsView implements IProjectsView, IListView<IProject> {
	id: string;
	constructor(id: string) {
		this.id = id;
	}
	template() {
		return `
		<div class="sidebar-projects">
			<h2 class="title-projects">Projects</h2>
			<button class="btn btn-newProject">New Project</button>
			<div class="modal"></div>
			<div class="item-projects"></div>
		</div>
		<div class="tasks"></div>
		`;
	}
	render() {
		let app = document.querySelector("main") as HTMLElement;
		let totos = document.createElement("section")! as HTMLElement;
		totos.classList.add("todos", "page");
		app = app.appendChild(totos);
		app.innerHTML = this.template();

		gotoPage(".todos");
	}
	getValueProjectName() {
		const projectName = (
			document.querySelector("#input-nameProject") as HTMLInputElement
		).value;
		return projectName;
	}
	renderList(e: Partial<IProject>[]) {
		const element = document.querySelector(".item-projects")! as HTMLElement;
		console.log(element);
		element.innerHTML = e
			.slice()
			.reverse()
			.map((project) => projectTemplate(project, this.id))
			.join("");
	}
	async addNewproject(e: Promise<Partial<IProject>>) {
		const element = document.querySelector(".item-projects")! as HTMLElement;
		const project = await e;
		element.innerHTML += projectTemplate(project, this.id);
	}
	addbtnLogout() {
		let app = document.querySelector("header") as HTMLElement;
		app.innerHTML += `
		<i class="fa fa-sign-out" id="btn-logout"></i>
		`;
	}
}
