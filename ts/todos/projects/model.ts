import { IProject, ITask } from "../../base/task.js";
import { IModel } from "../../base/index.js";
import { UserModel } from "../../user/userModel.js";
import { DBService } from "../../dbService/db.js";
interface IProjectsModel {
	userLogin: string;
	listProject: [IProject];
	addProject: (projectName: string) => void;
	projectByUserLogin: () => Promise<Partial<IProject>[]>;
	findProject: (nameProject: string) => Promise<Partial<IProject>>;
	findProjectById: (id: string) => Promise<Partial<IProject>>;
}
export class ProjectsModel implements IProjectsModel, IModel<IProject> {
	listProject: [IProject];
	userLogin: string;
	newDb?: DBService<IProject>;
	constructor() {
		this.userLogin = new UserModel().getUserLogin().username;
		this.newDb = new DBService<IProject>("development");
		this.listProject = this.newDb.getlist("PROJECTS") || [];
	}
	save(data: Partial<IProject>[]) {
		localStorage.setItem("PROJECTS", JSON.stringify(data));
		return true;
	}
	async addProject(projectName: string) {
		try {
			if (projectName) {
				const data = await this.listProject;
				data.push({
					id: "_" + Math.random().toString(36),
					projectName: projectName,
					userLogin: this.userLogin,
				});
				this.save(data);
			}
		} catch (err) {
			console.log(err);
		}
	}
	async projectByUserLogin(): Promise<Partial<IProject>[]> {
		try {
			const rs = await this.newDb?.findMany("PROJECTS", {
				userLogin: this.userLogin,
			});
			if (rs) {
				return rs;
			} else {
				throw new Error("error");
			}
		} catch (err) {
			throw err;
		}
	}
	async findProject(nameProject: string): Promise<Partial<IProject>> {
		try {
			const rs = await this.newDb?.findOne("PROJECTS", {
				projectName: nameProject,
			});
			if (rs) {
				return rs;
			} else throw new Error("error");
		} catch (err) {
			throw err;
		}
	}
	async findProjectById(id: string): Promise<Partial<IProject>> {
		try {
			const rs = await this.newDb?.findOne("PROJECTS", {
				id: id,
			});
			if (rs) {
				return rs;
			} else throw new Error("error");
		} catch (err) {
			throw err;
		}
	}
}
