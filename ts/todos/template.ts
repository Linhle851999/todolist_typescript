import { IProject, ITask } from "../base/task.js";
const projectTemplate = (element: Partial<IProject>, id: string) => {
	return `
        <div class="group-project-${id} group-project" id="group-project-${element.id}">
            <p class="name-project-task name-project-${id}" data-id="${element.id}">${element.projectName}</p>
            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
        </div>
    `;
};
const modalAddProject = () => {
	return `
        <div class="modal-content">
            <form class="form-addproject">
            <div class="group-addproject">
                <label class="name-Project">Name Project</label>
                <input type="text" class="input-nameProject" id="input-nameProject" name="input-nameProject">
            </div>
                <input class="btn btn-submit-addProject" type="submit" value="Submit">
            </form>
        </div>
    `;
};
const modalAddTask = () => {
	return `
        <div class="modal-content-addtask">
            <form class="form-addtask" name="form" id="form" novalidate>
                <legend class="title-form-addtask">New Task</legend>
                <div class="group-newtask group-nametask">
                    <label class="label-addtask name-newtask">Name Task:</label>
                    <input class="input-namenewtask input-nameaddtask" required=true minlength = "5">
                    <div class="error-messages single-error">
						<span class="error-message required-error"> Please enter this field !</span>
						<span class="error-message minlength-error">Length must be more than 5 characters<span>
					</div>
                </div>
                <div class="group-newtask group-datetask">
                    <label class="label-addtask date-newtask">Date:</label>
                    <input class="input-namenewtask input-dateaddtask" type="date" required=true>
                    <div class="error-messages single-error">
						<span class="error-message required-error"> Please enter this field !</span>
					</div>
                </div>
                <div class="group-newtask group-prioritytask">
                    <label class="label-addtask priority-newtask">Priority:</label>
                    <select class="input-namenewtask select-priority">
                        <option class="option-priority">High Priority</option>
                        <option class="option-priority">Medium Priority</option>
                        <option class="option-priority">Low Priority</option>
                    </select>
                </div>
                <div class="group-newtask group-descriptiontask">
                    <label class="label-addtask  description-newtask">Description:</label>
                    <textarea class="input-namenewtask textarea-description" required=true minlength = "5"></textarea>
                    <div class="error-messages single-error">
						<span class="error-message required-error"> Please enter this field !</span>
						<span class="error-message minlength-error">Length must be more than 5 characters<span>
					</div>
                </div>
                <input type="submit" class="btn btn-addtask" placeholder="Add">
            </form>
        </div>
    `;
};
const modalDetailTask = (e: Partial<ITask>) => {
	return `
	<div class="modal-content-addtask">
            <form class="form-addtask" name="form" id="form" novalidate>
                <legend class="title-form-addtask">Detail</legend>
                <div class="group-newtask group-nametask">
                    <label class="label-addtask name-newtask">Name Task:</label>
                    <input class="input-namenewtask input-nameaddtask" required=true minlength = "5" placeholder=${e.taksName}>
                    <div class="error-messages single-error">
											<span class="error-message required-error"> Please enter this field !</span>
											<span class="error-message minlength-error">Length must be more than 5 characters<span>
										</div>
                </div>
                <div class="group-newtask group-datetask">
                    <label class="label-addtask date-newtask">Date:</label>
                    <input class="input-namenewtask input-dateaddtask" type="date" required=true value=${e.time}>
                    <div class="error-messages single-error">
											<span class="error-message required-error"> Please enter this field !</span>
								</div>
                </div>
                <div class="group-newtask group-prioritytask">
                    <label class="label-addtask priority-newtask">Priority:</label>
                    <select class="input-namenewtask select-priority" value=${e.priority} >
                        <option class="option-priority">High Priority</option>
                        <option class="option-priority">Medium Priority</option>
                        <option class="option-priority">Low Priority</option>
                    </select>
                </div>
                <div class="group-newtask group-descriptiontask">
                    <label class="label-addtask  description-newtask">Description:</label>
                    <textarea class="input-namenewtask textarea-description" required=true minlength = "5" placeholder=${e.description}></textarea>
                    <div class="error-messages single-error">
											<span class="error-message required-error"> Please enter this field !</span>
											<span class="error-message minlength-error">Length must be more than 5 characters<span>
										</div>
                </div>
                <input type="submit" class="btn btn-detailtask btn-savetask" placeholder="Save">
								<input type="submit" class="btn btn-detailtask btn-donetask" placeholder="Mark Done/Not Done">
								<input type="submit" class="btn btn-detailtask btn-deletetask" placeholder="Delete">
            </form>
        </div>
	`;
};
const tasksTemplate = (projectName: string) => {
	return `
        <div class="nameProject">${projectName}</div>
        <button class="btn btn-newTask">New Task</button>
        <div class="modal-addTask"></div>
				<div class="group-tasks"></div>
    `;
};
const listTasks = (tasks: Partial<ITask>) => {
	return `
		<div class="group-task">
			<div class="name-task">${tasks.taksName}</div>
			<button class="btn btn-detail btn-detail-${tasks.id}"data-id=${tasks.id}>Details</button>
			<div class="modal-detailtask"></div>
    </div>
	`;
};
export {
	projectTemplate,
	modalAddProject,
	tasksTemplate,
	modalAddTask,
	listTasks,
	modalDetailTask,
};
