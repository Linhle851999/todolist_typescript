const localStorageService = {
	save: (resource: string, data: object): Promise<void> => {
		return new Promise<void>((rs) => {
			localStorage.setItem(resource, JSON.stringify(data));
			rs();
		});
	},
	get: (resource: string): Promise<object> => {
		return new Promise<object>((rs) => {
			const localStorageData = localStorage.getItem(resource);
			const data = localStorageData ? JSON.parse(localStorageData) : [];
			rs(data);
		});
	},
};
interface IDBService<T> {
	env: string;
	findOne: (resource: string, item: Partial<T>) => Promise<Partial<T>> ;
	saveOne: (resource: string, item: Partial<T>) => void;
	updateOne: (
		resource: string,
		item: Partial<T>,
		id: string
	) => Promise<boolean>;
	deleteOneById: (resource: string, id: string) => Promise<boolean>;
	findMany:(resource: string, item: Partial<T>) => Promise<Partial<T>[]>
	getlist:(resource: string) => T
}
export class DBService<T> implements IDBService<T> {
	env: string;
	constructor(env: string) {
		this.env = env;
	}
	getlist = (resource: string)=>{
		const db: any = this.env == "development" ? localStorageService : {};
		const data = db.get(resource)
		return data
	}
	findMany = (resource: string, item: Partial<T>): Promise<Partial<T>[]>=>{
		return new Promise<Partial<T>[]>((rs) => {
			const db: any = this.env == "development" ? localStorageService : {};
			db.get(resource).then((data: Partial<T>[]) => {
				const dataFilter: Partial<T>[] = []
				data.filter((a: Partial<T>) => {
					const keys = Object.keys(a);
					keys.map((key)=>{
						if (a[key as keyof typeof a] === item[key as keyof typeof item]) {
							dataFilter.push(a)
						}
					})
				})
				rs(dataFilter)
			});
		})

	}
	findOne = (resource: string, item: Partial<T> ) => {
		return new Promise<Partial<T>>((rs, rj) => {
			const db: any = this.env == "development" ? localStorageService : {};
			db.get(resource).then((data: Partial<T>[]) => {
				data.find((a: Partial<T>) => {
					const keys = Object.keys(a);
					keys.map((key)=>{
						if (a[key as keyof typeof a] === item[key as keyof typeof item]) {
							rs(a);
						}
					})
				});
				rj()
			});
		});
	}
	saveOne = (resource: string, item: Partial<T>) => {
		return new Promise<void>((rs) => {
			const db: any = this.env == "development" ? localStorageService : {};
			db.get(resource).then((data:Partial<T>[]) => {
				if (data) {
					data.push(item);
				}
				db.save(resource, data);
				rs();
			});
		});
	};
	deleteOneById = (resource: string, id: string) => {
		return new Promise<boolean>((rs, rj) => {
			const db: any = this.env == "development" ? localStorageService : {};
			const data = db.get(resource);
			const items = data.filter((item: { id: string }) => item.id != id);
			if (items) {
				db.save(resource, items);
				rs(true);
			} else {
				rj(false);
			}
		});
	};
	updateOne = (resource: string, item: Partial<T>) => {
		return new Promise<boolean>((rs, rj) => {
			if (item) {
				this.saveOne(resource, item);
				rs(true);
			} else {
				rj(false);
			}
		});
	};
}