export interface IModel<T> {
	save?: (data: Partial<T>[]) => boolean;
	update?: (data: Partial<T>[]) => boolean;
	delete?: () => boolean;
	get?: () => object;
}

export interface IListModel<T> {
	fetch?: () => T[];
	findByid?: (id: string) => T;
	deleteOneById?: (id: string) => boolean;
	updateOneById?: (id: string) => boolean;
    addOne?:(item: T)=> boolean
}
