export enum EPriority {
	High = "High Priority",
	Medium = "Medium Priority",
	Low = "Low Priority",
}
export interface ITask {
	id: string;
	idProject: string;
	projectName: string;
	taksName: string;
	time: Date;
	priority: string;
	description: string;
}
export interface IProject {
	id: string;
	projectName: string;
	userLogin: any;
	tasks?: [ITask];
}
