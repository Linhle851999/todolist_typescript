export interface IView{
	template?: () => string;
	render?: () => void;
	queryOne?: () => HTMLElement;
	queryMany?: () => HTMLElement[];
}
export interface IListView<T>{
    id: string;
    renderList?: (list: T[])=> void
}