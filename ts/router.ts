const gotoPage = (pageSelector: string, id?: string, callback?: any) => {
    const pages = document.querySelectorAll('.page');
    pages.forEach(element => {
        element.classList.remove('active');
    });

    const page = document.querySelector(pageSelector) as HTMLElement | null;

    page?.classList.add('active');

    if (id) {
        window.history.replaceState({}, document.title, `?id=${id}`);

    }

    callback && callback();
}

export { gotoPage }
